const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

// With JSDoc @type annotations, IDEs can provide config autocompletion
/** @type {import('@docusaurus/types').DocusaurusConfig} */
(module.exports = {
  title: 'JAE',
  tagline: 'Just Another Engineer',
  url: 'https://justanother.engineer/',
  trailingSlash: false,
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'jae',
  projectName: 'jae',

  presets: [
    [
      '@docusaurus/preset-classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl: 'https://gitlab.com/-/ide/project/justanotherengineer/jae/edit/master/-/',
          showLastUpdateTime: true,
          showLastUpdateAuthor: true,
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
        gtag: {
          trackingID: 'G-N556PF9XWK',
          anonymizeIP: true,
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      metadata: [{name: 'keywords', content: 'AWS, Linux, Terraform, Packer, databases, infrastructure management, Vault, Consul, cheatsheets, cheatsheet, DevOps, SysAdmin, SRE, Engineering, CLI cheatsheet, terminal commands, CLI arguments, parameters, tech topics, command line interface'}],
      docs: {
        sidebar: {
          hideable: true,
        },
      },
      colorMode: {
        defaultMode: 'dark',
        disableSwitch: false,
        respectPrefersColorScheme: true,
      },
      algolia: {
        apiKey: '902a872ced32cf57b778bddafda5c656',
        indexName: 'jae',
	      searchParameters: {},
        contextualSearch: false,
        appId: 'WHPHTCRDVW',
      },
      navbar: {
        title: '',
        logo: {
          alt: 'My Site Logo',
          src: 'img/logo.svg',
        },
        items: [
          {
            type: 'doc',
            docId: 'introduction',
            position: 'left',
            label: 'Cheatsheets',
          },
          {
            to: 'contributing',
            position: 'left',
            label: 'Contributing',
          },
          {
            type: 'dropdown',
            label: 'Community',
            position: 'left',
            items: [
		    {
       		        href: 'https://join.slack.com/t/justanotherengineer/shared_invite/zt-23m74nwgy-STbP1b2nskLTz5Ql3NRtTQ',
			    label: 'Slack'
		    },
		    {
                           href: 'https://www.linkedin.com/company/just-another-engineer/',
			    label: 'LinkedIn'
		    },
	    ],
          },
          {
            to: 'about',
            position: 'left',
            label: 'About',
          },
          {
            href: 'https://www.buymeacoffee.com/luiz1361',
            label: 'Buy me a ☕',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'light',
        links: [
          {
            title: 'Community',
            items: [
              {
                label: 'Contributing',
                to: 'contributing',
              },
              {
                label: 'GitLab Issues',
                href: 'https://gitlab.com/justanotherengineer/jae/-/issues',
              },
              {
                label: 'Source Code',
                href: 'https://gitlab.com/justanotherengineer/jae',
              },
              {
                label: 'Slack',
                href: 'https://join.slack.com/t/justanotherengineer/shared_invite/zt-23m74nwgy-STbP1b2nskLTz5Ql3NRtTQ',
              },
              {
                label: 'LinkedIn',
                href: 'https://www.linkedin.com/company/just-another-engineer/',
              },
            ],
          },
          {
            title: 'Legal',
            items: [
              {
                label: 'Privacy Policy',
                to: 'privacy',
              },
              {
                label: 'Cookie Policy',
                to: 'cookie_policy',
              },
              {
                label: 'Comment Policy',
                to: 'comment_policy',
              },
              {
                label: 'Code of Conduct',
                to: 'code_of_conduct',
              },
              {
                label: 'License',
                to: 'license',
              },
            ],
          },
          {
            title: 'More',
            items: [
              {
                label: 'Pingdom',
                href: 'http://stats.pingdom.com/ieq3v42yov76?_gl=1*1073t62*_gcl_au*MTI2MDgwMDc0My4xNjk1MTU3NTc1',
              },
              {
                label: 'Sitemap',
                href: 'https://justanother.engineer/sitemap.xml',
              },
              {
                label: 'About',
                to: 'about',
              },
            ],
          },
          {
            title: 'Credit',
            items: [
              {
                label: 'GitLab 🧡',
                href: 'https://about.gitlab.com/solutions/open-source/join/',
              },
              {
                label: 'Algolia 💙',
                href: 'https://www.algolia.com/for-open-source/',
              },
              {
                label: 'Cloudflare',
                href: 'https://www.cloudflare.com/',
              },
              {
                label: 'Disqus',
                href: 'https://disqus.com/',
              },
              {
                label: 'Docusaurus',
                href: 'https://docusaurus.io/',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Just Another Engineer.`,
      },
      prism: {
        theme: lightCodeTheme,
	additionalLanguages: ['powershell','php'],
        darkTheme: darkCodeTheme,
      },
    }),
});
