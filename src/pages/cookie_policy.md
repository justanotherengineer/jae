# Cookie Policy

## Introduction

Welcome to [Just Another Engineer] ("we," "our," or "us"). This Cookie Policy is designed to inform you about the use of cookies and similar tracking technologies on our website.

## What Are Cookies?

Cookies are small text files that are placed on your computer, smartphone, or other device when you visit a website. They are widely used to make websites work or to improve their efficiency, as well as to provide information to website owners.

## Types of Cookies We Use

We use various types of cookies on our website, including but not limited to:

### Essential Cookies

These cookies are necessary for the website to function properly. They enable you to navigate our site and use its features.

### Performance Cookies

These cookies collect information about how visitors use our website. They help us improve the website's performance and provide a better user experience.

### Functionality Cookies

These cookies allow the website to remember choices you make (e.g., language preferences) and provide enhanced, more personalized features.

### Advertising/Marketing Cookies

These cookies are used to deliver advertisements that are relevant to your interests. They may also limit the number of times you see an advertisement and help measure the effectiveness of advertising campaigns.

### Analytics Cookies

Analytics cookies help us understand how our website is being used, such as which pages are visited most frequently, the duration of visits, and other user behavior patterns. We use this information to improve our website and user experience.

### Social Media Cookies

Social media cookies allow you to share content from our website on social media platforms. They may also track your interactions with embedded social media content on our site.

### Third-Party Cookies

We may use third-party services on our website that may set cookies on your device. These third-party cookies are subject to the respective privacy policies of those services, and we recommend reviewing their policies for more information.

## Cookie Consent

When you visit our website for the first time, you will be presented with a cookie banner that requests your consent to use cookies. By continuing to use our website, you consent to our use of cookies as described in this policy, unless you disable cookies in your browser settings.

## Managing Cookies

You can control and manage cookies in various ways, depending on your browser settings. Please note that disabling cookies may impact your experience on our website and limit its functionality.

## Contact Us

If you have any questions or concerns about our Cookie Policy or the use of cookies on our website, please [contact us](mailto:luiz@justanother.engineer).

## Changes to This Policy

We may update our Cookie Policy from time to time. Any changes will be posted on this page, along with the date of the last update. We encourage you to review this policy periodically to stay informed about how we use cookies.
