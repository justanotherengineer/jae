# Python

## Introduction

This cheat sheet provides a quick reference for some common Python programming commands and concepts. Python is a versatile and widely-used programming language known for its simplicity and readability.

## Basic Python Program Structure

A typical Python program has the following structure:

```python
# Comments and explanations
# ...

# Variables
variable_name = value

# Functions
def function_name(param1, param2):
    # Function body
    return some_value

if __name__ == "__main__":
    # Main function body
    print("Hello, World!")
```

## Variables

- Declare and initialize a variable:
  ```python
  variable_name = value
  ```

## Data Types

- Basic data types in Python include `int`, `float`, `str`, `bool`, and more.

- Complex data types like `list`, `tuple`, `dict`, and `set` are used for custom data structures.

## Functions

- Define a function:
  ```python
  def function_name(param1, param2):
      # Function body
      return some_value
  ```

- Call a function:
  ```python
  result = function_name(arg1, arg2)
  ```

## Control Flow

- Use `if` statements for conditional execution:
  ```python
  if condition:
      # Code to execute if condition is True
  else:
      # Code to execute if condition is False
  ```

- Use `for` loops for iteration:
  ```python
  for i in range(count):
      # Code to repeat
  ```

- Use `while` loops for indefinite iteration:
  ```python
  while condition:
      # Code to repeat
  ```

## Lists and Dictionaries

- Create a list:
  ```python
  my_list = [value1, value2, ...]
  ```

- Create a dictionary:
  ```python
  my_dict = {"key1": value1, "key2": value2, ...}
  ```

## Packages and Imports

- Import packages for external functionality:
  ```python
  import math
  ```

- Create and organize your own modules and packages within your project.

## Exception Handling

- Handle exceptions using `try`, `except`, and `finally` blocks:
  ```python
  try:
      # Code that might raise an exception
  except SomeException:
      # Code to handle the exception
  finally:
      # Code to execute regardless of whether an exception was raised
  ```

## Classes and Objects

- Define a class:
  ```python
  class MyClass:
      def __init__(self, param1, param2):
          # Constructor
          self.param1 = param1
          self.param2 = param2

      def my_method(self):
          # Class method
          return some_value
  ```

## File Handling

- Open and manipulate files:
  ```python
  with open("filename.txt", "r") as file:
      data = file.read()
  ```

## Conclusion

This cheat sheet covers some common Python programming commands and concepts. Python is widely used for web development, data analysis, and more; refer to the [official Python documentation](https://docs.python.org/3/) for more in-depth information and advanced usage.
